import { MyAlphaNumberT, MyDisplayAlphaT, MyIsPosiNegT, MySizeAlphaT, fibo, sum } from "../src";
//import { MyArrayAlphaT, MyDisplayAlphaReverseT, MyLengthArrayT, MyDisplayUnicodeT } from "../src";

describe('index', () => {
  describe('MyAlphaNumberT', () => {
    it('Should be test return a number in string', () => {
      expect(MyAlphaNumberT(23)).toBe('23');
    });
  });
  describe('sum', () => {
    it('Should be test return a sum between a and b', () => {
      expect(sum(4, 5)).toBe(9);
    });
    it('Should be test return a 0 because one != number', () => {
      expect(sum('4', 5)).toBe(0);
    });
    it('Should be test return a 0 because there is no number', () => {
      expect(sum('4', '5')).toBe(0);
    });
  });
  describe('MySizeAlphaT', () => {
    it('Should be test return a count with a string', () => {
      expect(MySizeAlphaT('hello')).toBe(5);
    });
    it('Should be test return a count with an empty string', () => {
      expect(MySizeAlphaT('')).toBe(0);
    });
    it('Should be test return a count with a number', () => {
      expect(MySizeAlphaT(4)).toBe(0);
    });
  });
  describe('MyDisplayAlphaT', () => {
    it('Should be test return the alphabet', () => {
      expect(MyDisplayAlphaT()).toBe('abcdefghijklmnopqrstuvwxyz');
    });
  });
    /*
  describe('MyArrayAlphaT', () => {
    it('Should be test return a table with all letter of a word one by one', () => {
      expect(MyArrayAlphaT('hello')).toBe(['h', 'e', 'l', 'l', 'o']);
    });
    it('Should be test return an empty table', () => {
      expect(MyArrayAlphaT(42)).toBe([]);
    });
  });
  */
  describe('MyIsPosiNegT', () => {
    it('Should be test return a positive number', () => {
      expect(MyIsPosiNegT(4)).toBe('POSITIF');
    });
    it('Should be test return a negative number', () => {
      expect(MyIsPosiNegT(-2)).toBe('NEGATIVE');
    });
    it('Should be test return a positive value with a string', () => {
      expect(MyIsPosiNegT('hello')).toBe('POSITIF');
    });
  });
  describe('fibo', () => {
    it('Should be test return a sum', () => {
      expect(fibo(6)).toBe(8);
    });
    it('Should be test return 0', () => {
      expect(fibo(-4)).toBe(0);
    });
    it('Should be test return 1', () => {
      expect(fibo(2)).toBe(1);
    });
  });
    /*
  describe('MyDisplayAlphaReverseT', () => {
    it('Should be test return a alphabet reverse', () => {
      expect(MyDisplayAlphaReverseT()).toBe('zyxwvutsrqponmlkjihgfedcba');
    });
  });
  */
  /*
  describe('MyLengthArrayT', () => {
    it('Should be test return a 1', () => {
      expect(MyLengthArrayT(['test'])).toBe(1);
    });
    it('Should be test return a 0', () => {
      expect(MyLengthArrayT()).toBe(0);
    });
  });
  */
  /*
  describe('MyDisplayUnicodeT', () => {
    it('Should be test return letters in a string', () => {
      expect(MyDisplayUnicodeT([65, 66, 67])).toBe('ABC');
    });
    it('Should be test return numbers in a string', () => {
      expect(MyDisplayUnicodeT([48, 49, 50])).toBe('012');
    });
  });
  */
});